Projet: house
Auteurs: Michael K�lling and David J. Barnes
Traduction: Laurent Pierron

Ce projet fait partie du mat�riel pour le livre

   Objects First with Java - A Practical Introduction using BlueJ
   Fifth edition
   David J. Barnes and Michael Kölling
   Pearson Education, 2012

Il est expliqu� dans le chapitre 1.

C'est un projet tr�s simple pour montrer certaines propri�t�s des objets.

Vous pouvez cr�er diff�rentes formes, et vous verrez, si vous le faites,
que ces formes sont dessin�es sur l'�cran (dans une fen�tre que nous
appelons le `canvas`).

Vous pouvez manipuler ces objets : changez leur position, taille et couleur.
Essayez : cr�ez quelques carr�s, triangles et cercles diff�rents.

Ce projet est con�u comme un premier exemple de programmation orient�e-objet.
Il illustre plusieurs concepts :

 - un projet Java (application) est une collection de classes
 - des objets peuvent �tre cr��s � partir de classes
 - d'une classe quelconque peut �tre cr�� de nombreux objets
 - les objets ont des op�rations (m�thodes)
 - les op�rations peuvent avoir des param�tres
 - les param�tres ont des types (au moins String et int)
 - les objets poss�dent des donn�es (champs)
 - les op�rations et les champs sont communs � tous les objets de la m�me classe
 - les valeurs stock�es dans les champs peuvent diff�r�es pour chaque objet

Le projet montre aussi

 - la cr�ation d'objet avec BlueJ
 - l'appel interactif de m�thode
 - le passage de param�tre

Un bon second projet � consulter ensuite est `house`, qui ajoute une classe
� celles de ce projet. Cette classe (nomm�e `Picture`) utilise les formes
pour dessiner une image. Il peut �tre utilis� pour exp�rimenter le codage.