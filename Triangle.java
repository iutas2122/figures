import java.awt.*;

/**
 * Un triangle qui peut �tre manipul� et qui se dessine lui-m�me � l'�cran.
 * 
 * @author  Michael K�lling and David J. Barnes
 * @version 2011.07.31
 */

public class Triangle implements Movable
{
    private int height;
    private int width;
    private int xPosition;
    private int yPosition;
    private String color;
    private boolean isVisible;

    /**
     * Cr�e un nouveau triangle � la position par d�faut avec la couleur par d�faut.
     *
     */
    public Triangle()
    {
        height = 60;
        width = 70;
        xPosition = 210;
        yPosition = 140;
        color = "green";
        isVisible = false;
    }

    /**
     * Rend ce triangle visible. Si il �tait d�j� visible, ne fait rien.
     *
     */
    public void makeVisible()
    {
        isVisible = true;
        draw();
    }

    /**
     * Rend ce triangle visible. Si il �tait d�j� visible, ne fait rien.
     *
     */
    public void makeInvisible()
    {
        erase();
        isVisible = false;
    }

    /**
     * D�place le triangle de quelques pixels � droite.
     *
     */
    public void moveRight()
    {
        moveHorizontal(20);
    }

    /**
     * D�place le triangle de quelques pixels � gauche.
     *
     */
    public void moveLeft()
    {
        moveHorizontal(-20);
    }

    /**
     * D�place le triangle de quelques pixels vers le haut.
     *
     */
    public void moveUp()
    {
        moveVertical(-20);
    }

    /**
     * D�place le triangle de quelques pixels vers le bas.
     *
     */
    public void moveDown()
    {
        moveVertical(20);
    }

    /**
     * D�place le triangle horizontalemnt de 'distance' pixels.
     *
     * @param distance La longueur du d�placement en pixels.
     */
    public void moveHorizontal(int distance)
    {
        erase();
        xPosition += distance;
        draw();
    }

    /**
     * D�place le triangle verticalement de 'distance' pixels.
     *
     * @param distance La longueur du d�placement en pixels.
     */
    public void moveVertical(int distance)
    {
        erase();
        yPosition += distance;
        draw();
    }


    /**
     * Remplace la taille actuelle du triangle par la nouvelle taille.
     *
     * @param newHeight La nouvelle hauteur en pixels. Doit �tre &gt;= 0.
     * @param newWidth La nouvelle largeur en pixels. Doit �tre &gt;= 0.
     */
    public void changeSize(int newHeight, int newWidth)
    {
        erase();
        height = newHeight;
        width = newWidth;
        draw();
    }

    /**
     * Change la couleur.
     *
     * @param newColor La nouvelle couleur, les valeurs possibles sont "red",
     * "yellow", "blue", "green", "magenta" and "black".
     */
    public void changeColor(String newColor)
    {
        color = newColor;
        draw();
    }

    /**
     * Dessine le triangle � l'�cran avec les caract�ristiques actuelles.
     */
    private void draw()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            int[] xpoints = { xPosition, xPosition + (width/2), xPosition - (width/2) };
            int[] ypoints = { yPosition, yPosition + height, yPosition + height };
            canvas.draw(this, color, new Polygon(xpoints, ypoints, 3));
            canvas.wait(10);
        }
    }

    /**
     * Retire le triangle de l'�cran.
     */
    private void erase()
    {
        if(isVisible) {
            Canvas canvas = Canvas.getCanvas();
            canvas.erase(this);
        }
    }
}
