import java.lang.Math;

/**
 * D�crivez votre interface Movable ici.
 *
 * @author  (votre nom)
 * @version (un num�ro de version ou une date)
 */

public interface Movable
{
    /**
     * M�thodes abstraites
     */
    void moveHorizontal(int distance);
    void moveVertical(int distance);
    
    /**
     * D�place horizontalement et lentement le triangle  de 'distance' pixels.
     *
     * @param distance La longueur du d�placement en pixels.
     */
    default void slowMoveHorizontal(int distance)
    {
        int delta = (distance > 0) ? 1 : -1;
        int steps = Math.abs(distance);

        for(int i = 0; i < steps; i++)
        {
            moveHorizontal(delta);
        }
    }

    /**
     * D�place horizontalement et lentement le triangle  de 'distance' pixels.
     *
     * @param distance La longueur du d�placement en pixels.
     */
    default void slowMoveVertical(int distance)
    {
        int delta = (distance > 0) ? 1 : -1;
        int steps = Math.abs(distance);

        for(int i = 0; i < steps; i++)
        {
            moveVertical(delta);
        }
    }
}
